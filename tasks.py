__author__ = 'jnewnham'

import webapp2
from handlers import *

app = webapp2.WSGIApplication([
        ('/tasks/processuser', ProcessUserTaskHandler),
        ('/tasks/analyseusers', AnalyseUserTaskHandler),
        ('/tasks/refreshproducts', RefreshProductTaskHandler),
        ('/tasks/updateusers', RefreshProductTaskHandler),
        ('/tasks/csvuploader', CSVUploaderHandler),
        ('/tasks/processskuitems', ProcessProductSKUsTaskHandler),
    ], debug=True)