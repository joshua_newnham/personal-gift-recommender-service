__author__ = 'jnewnham'

import json
import requests
import urllib
import urllib2
from utils import *
from urllib import urlencode

ALCHEMYAPI_BASE_URL = 'http://access.alchemyapi.com/calls'
ALCHEMYAPI_API_KEY = "160674cf4b838ce76654a833fa0dbe09ff9a096b"
ALCHEMYAPI_TAXONOMY_TEXT = ALCHEMYAPI_BASE_URL + "/text/TextGetRankedTaxonomy"
ALCHEMYAPI_TAXONOMY_HTML = ALCHEMYAPI_BASE_URL + "/html/HTMLGetRankedTaxonomy"

FB_TOKEN = "CAAGGLUNJLwcBAOgupzQZAAa3FLEVfpZCexI8c1AjfHRC61KD4TzDxPhM8gLUy40sWAZAZBuCdvHZC1B27oFX75fnGCiLlhxeJYIKTqMYPvqZC5DZCLXe5YKtRD7BsO3PzZB2cLKvkZAxceWXFh8ibxITQekxo6x2nIVJFcDURYhrvNOgIqp6YVb5t"

FB_BASE_URL = 'https://graph.facebook.com/v1.0/'


def alchemy_api(url, query, type='text'):
    params = {}
    params['apikey'] = ALCHEMYAPI_API_KEY
    params['outputMode'] = 'json'
    params[type] = query

    post_url = url + '?' + urlencode(params).encode('utf-8')

    print post_url

    return requests.get(post_url).json()

class Interest:
    fb_category = ""
    fb_name = ""
    taxonomy = []


def test0():
    fields = 'id,bio,name,birthday,email,about,gender,relationship_status,interests,friends.fields(id,name,bio,birthday,email,gender,relationship_status,interests,about)'
    url = '%sme?fields=%s&access_token=%s' % (FB_BASE_URL, fields, FB_TOKEN,)

    print url

    content = requests.get(url).json()

    interests = []

    age = 0
    if "birthday" in content:
        age = textutils.calculate_age(content["birthday"])
        print str(age)

    if "interests" in content:
        interests = getInterests(content["interests"], True)

        print "interests collected " + str(len(interests))

    #print json.dumps(content, indent=1)

    fields = 'id,bio,name,birthday,about,gender,relationship_status,interests,email'
    url = '%sme/friends?fields=%s&access_token=%s' % (FB_BASE_URL, fields, FB_TOKEN,)

    print url

    content = requests.get(url).json()
    print json.dumps(content, indent=1)

def getInterests(json_packet, include_taxonomy=False):
    interests = []

    next_url = None
    if "paging" in json_packet:
        if "next" in json_packet["paging"]:
            next_url = json_packet["paging"]["next"]

    if "data" in json_packet:
        json_data = json_packet["data"]

        for json_data_item in json_data:
            interest = Interest()
            interest.fb_category = json_data_item["category"]
            interest.fb_name = json_data_item["name"]

            if include_taxonomy:
                print json.dumps(alchemy_api(ALCHEMYAPI_TAXONOMY_TEXT, interest.fb_name, "text"), indent=1)


            interests.append(interest)

    if next_url is not None:
        content = requests.get(next_url).json()
        content_results = getInterests(content)
        if content_results is not None and len(content_results) > 0:
            interests.extend(content_results)

    return interests

test0()


print textutils.nltk_fuzzy_match("Joshua Newnh", "joshua newnham")

