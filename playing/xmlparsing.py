__author__ = 'jnewnham'

import urllib
import urllib2
import json
import time
import re
from datetime import *
from alchemyapi.alchemyapi import AlchemyAPI
#from xml.etree import ElementTree
from bs4 import *

xml_sample = """<?xml version="1.0"?><prd:Product xmlns:prd="http://schemas.homeretailgroup.com/product" xmlns:prm="http://schemas.homeretailgroup.com/promotion" xmlns:cst="http://schemas.homeretailgroup.com/customer" xmlns:cmn="http://schemas.homeretailgroup.com/common" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://schemas.homeretailgroup.com/product/product-v1.xsd" version="1" uri="http://api.homeretailgroup.com/product/argos/8615710" id="8615710" brand="Argos"><prd:Brand>Living</prd:Brand><prd:ShortDescription><![CDATA[Living Non-Stick 12 Cup Muffin Tray.]]></prd:ShortDescription><prd:LongDescription><![CDATA[Treat yourself to a freshly baked muffin from this Living Teflon non-stick muffin tray. 1 muffin tray L35cm. General information: Made from steel. Oven safe up to 230C. Teflon non-stick coating. Dishwasher safe. EAN: 8615710. ]]></prd:LongDescription><prd:LongDescriptionHtml><![CDATA[<p>Treat yourself to a freshly baked muffin from this Living Teflon non-stick muffin tray.</p><ul><li>1 muffin tray L35cm.</li></ul><p>General information:</p><ul><li>Made from steel.</li><li>Oven safe up to 230&deg;C.</li><li>Teflon non-stick coating.</li><li>Dishwasher safe.</li><li>EAN: 8615710.</li></ul>]]></prd:LongDescriptionHtml><prd:PurchasingOptions><prd:Option type="inStoreReservable"><prd:Commentary type="statusMessage">Available to reserve in store, subject to stock</prd:Commentary><prd:MaximumQuantity>10</prd:MaximumQuantity></prd:Option><prd:Option type="homeDelivery" inStock="true"><prd:LeadTime units="days">2</prd:LeadTime><prd:Commentary type="statusMessage"><![CDATA[Home delivery within 2 days]]></prd:Commentary><prd:MaximumQuantity>10</prd:MaximumQuantity><prd:Cost currency="GBP">3.95</prd:Cost></prd:Option></prd:PurchasingOptions><prd:PricingInformation><prd:CurrentPrice><prd:Price currency="GBP">4.49</prd:Price><prd:Commentary type="footnote">Prices correct as displayed but are subject to change</prd:Commentary><prd:Tax type="VAT" currency="GBP" rate="0.20"></prd:Tax></prd:CurrentPrice></prd:PricingInformation><prd:Rating><prd:AverageRating>4.8</prd:AverageRating><prd:NumberReviews>49</prd:NumberReviews></prd:Rating><prd:AssociatedMedia><prd:Content index="0" type="image/jpeg" href="http://www.argos.co.uk/wcsstore/argos/images/113-8615710A75UC899886X.jpg" usage="largeImage" provider="Home Retail Group"></prd:Content><prd:Content index="1" type="image/jpeg" href="http://www.argos.co.uk/wcsstore/argos/images/113-8615710A75UC899886M.jpg" usage="mediumImage" provider="Home Retail Group"></prd:Content><prd:Content index="2" type="image/jpeg" href="http://www.argos.co.uk/wcsstore/argos/images/113-8615710A75UC899886T.jpg" usage="thumbnail" provider="Home Retail Group"></prd:Content><prd:Content index="3" type="image/jpeg" href="http://argos.scene7.com/is/image/Argos/8615710_R_Z001A_UC899886" usage="image" provider="Scene7"/><prd:Content index="4" type="image/jpeg" href="http://argos.scene7.com/is/image/Argos/8615710_R_Z002A_UC1514258" usage="image" provider="Scene7"/><prd:Content index="5" type="image/jpeg" href="http://argos.scene7.com/is/image/Argos/8615710_R_Z003A_UC1514259" usage="image" provider="Scene7"/></prd:AssociatedMedia><prd:RelatedProducts><prd:Product version="1" uri="http://api.homeretailgroup.com/product/argos/8617347" id="8617347" brand="Argos" type="alternative" index="0">8617347</prd:Product></prd:RelatedProducts></prd:Product>"""

def parse_product(xml_as_text):
    product = {}

    #tree = ElementTree.fromstring(xml_as_text)
    #print tree
    #print tree.findtext(".//prd/:Product")
    soup = BeautifulSoup(xml_as_text)
    print soup._is_xml

    #product["id"] = root.attrib["id"]
    #product["url"] = root.attrib["url"]
    #product["brand"] = root.attrib["brand"]
    #
    #for elem in tree.iterfind('prd:Brand'):
    #    print elem.text




parse_product(xml_sample)