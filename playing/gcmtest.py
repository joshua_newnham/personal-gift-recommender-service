__author__ = 'jnewnham'

import urllib
import urllib2
import json
import time
import re
from datetime import *
from alchemyapi.alchemyapi import AlchemyAPI
from urllib import urlencode
import requests

def send_google_cloud_message(gcm_reg_id, message):

    if gcm_reg_id is None or len(gcm_reg_id) == 0:
        return False

    resultContent = ""

    bodyFields =  {
        "data": {"message": message},
        "registration_ids": [gcm_reg_id]
    }

    #result = urlfetch.fetch(url="https://android.googleapis.com/gcm/send",
    #                        payload=json.dumps(bodyFields),
    #                        method=urlfetch.POST,
    #                        headers= {'Content-Type': 'application/json', 'Authorization': 'key=' + globals.GOOGLE_CLOUD_MESSAGING_API})

    request_object = urllib2.Request(
        url="https://android.googleapis.com/gcm/send",
        data=json.dumps(bodyFields),
        headers={'Content-Type': 'application/json', 'Authorization': 'key=AIzaSyBqXDEyx0CFd5oSqjSZ7sqLqF2Ry2GQL8o'})

    print "Calling " + request_object.get_full_url()

    try:
        response = urllib2.urlopen(request_object)
        print response.read()
    except urllib2.HTTPError as e:
        print e

    return True

# 06-27 16:25:55.041: I/GoogleCloudMessagingManager(30298): Device registered, registration ID=APA91bFlAps_Xn-aTBYBFi1oeBjFwSteHd5bHfk4nlGJsZUvrYfKkTRjUdMc_HnMDIb5b80XGaLZdIRQLusdhnvhj9pp7ENj36gD6lYvJZbaRUKFUAvAVTGOUPvHhicF13lHHHOh4Yi8P_Gqtl7Vv-VDsjFQHIOHuQ

send_google_cloud_message('APA91bHLj6kfJwlkIkuAt3wzg3bIiyEMCRqvl1EOcChdeuSFEThJ1wYk-TnuOXqfPjCr_BhfUcS4hMi4vXmuXX4pvPkYzeXP1QLaOekehClr1aQrgAt6t34gc7qpmcaMXY5SNOfhaD0cL9jifdvs1b-ChfnSJaaUuA', 'hello world')

#06-27 16:44:33.064: I/GoogleCloudMessagingManager(32528): Device registered, registration ID=
