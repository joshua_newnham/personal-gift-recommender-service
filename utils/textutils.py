__author__ = 'jnewnham'

import string
from nltk import metrics
from datetime import date
from datetime import datetime

# http://streamhacker.com/2011/10/31/fuzzy-string-matching-python/

def normalize(s):
    for p in string.punctuation:
        s = s.replace(p, '')

    return s.lower().strip()


def fuzzy_match(s1, s2):
    '''
    >>> fuzzy_match('Happy Days', ' happy days ')
    True
    >>> fuzzy_match('happy days', 'sad days')
    False
    '''
    return normalize(s1) == normalize(s2)

def nltk_fuzzy_match(s1, s2, max_dist=3):
    return metrics.edit_distance(normalize(s1), normalize(s2)) <= max_dist

# http://stackoverflow.com/questions/2217488/age-from-birthdate-in-python
def calculate_age(born_date_as_string):

    try:
        born = datetime.strptime(born_date_as_string, "%m/%d/%Y")
    except ValueError:
        return 0

    today = datetime.today()
    try:
        birthday = born.replace(year=today.year)
    except ValueError: # raised when birth date is February 29 and the current year is not a leap year
        birthday = born.replace(year=today.year, month=born.month+1, day=1)
    if birthday > today:
        return today.year - born.year - 1
    else:
        return today.year - born.year