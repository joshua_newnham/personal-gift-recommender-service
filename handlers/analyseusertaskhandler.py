__author__ = 'jnewnham'

from basetaskhandler import BaseTaskHandler
from model import *
from google.appengine.api import urlfetch
from google.appengine.api import memcache
from google.appengine.api.labs import taskqueue
import json
import logging
import globals

class AnalyseUserTaskHandler(BaseTaskHandler):
    def get(self):
        logging.info("AnalysisUserTaskHandler.get")
        self.post()

    def post(self):
        logging.info("AnalysisUserTaskHandler.post")

        gcm_reg_id = self.request.get("gcm_reg_id", default_value=None)

        userAccountTokens = UserAccountToken.getAll()

        if userAccountTokens is None or len(userAccountTokens) == 0:
            logging.info("AnalysisUserTaskHandler.post; no accounts with tokens found")
            return

        for userAccountToken in userAccountTokens:
            if self.userAnalysisRequired(userAccountToken):
                self.userAnalysis(userAccountToken)


        # notify user that the task has finished
        if gcm_reg_id is not None:
            logging.info("AnalyseUserTaskHandler.send_google_cloud_message: " + gcm_reg_id)
            self.send_google_cloud_message(gcm_reg_id, "analysising_contacts_finished")



    def userAnalysisRequired(self, userAccountToken):
        logging.info("AnalysisUserTaskHandler.userAnalysisRequired")

        user = userAccountToken.user

        if self.getCurrentTimeInSecs() - user.analysis_timestamp > globals.USER_ANALYSIS_REFRESH:
            return True

        if userAccountToken.account == globals.ACCOUNT_FACEBOOK and (user.fb_id is None or len(user.fb_id) == 0):
            return True

        if userAccountToken.account == globals.ACCOUNT_TWITTER and (user.twitter_handle is None or len(user.twitter_handle) == 0):
            return True

        if userAccountToken.account == globals.ACCOUNT_LINKEDIN and (user.linkedin_id is None or len(user.linkedin_id) == 0):
            return True

        return False


    def userAnalysis(self, userAccountToken):
        logging.info("AnalysisUserTaskHandler.userAnalysis")

        user = userAccountToken.user
        if userAccountToken.account == globals.ACCOUNT_FACEBOOK:
            self.userAnslysisFacebook(user, userAccountToken.token)

        if userAccountToken.account == globals.ACCOUNT_TWITTER:
            logging.warning("twitter analysis not implemented")

        if userAccountToken.account == globals.ACCOUNT_LINKEDIN:
            logging.warning("linkedin analysis not implemented")

        user.analysis_timestamp = self.getCurrentTimeInSecs()
        user.put()

    def userAnslysisFacebook(self, user, token):
        logging.info("AnalysisUserTaskHandler.userAnslysisFacebook")

