__author__ = 'jnewnham'

from basehandler import BaseHandler
import webapp2
import logging
from google.appengine.api import urlfetch
import globals
import json


class BaseTaskHandler(BaseHandler):
    def get(self):
        self.get()

    def post(self):
        self.post()

    def send_google_cloud_message(self, gcm_reg_id, message):
        logging.info("send_google_cloud_message")

        if gcm_reg_id is None or len(gcm_reg_id) == 0:
            return False

        resultContent = ""

        bodyFields =  {
              "data": {"message": message},
              "registration_ids": [gcm_reg_id]
        }

        result = urlfetch.fetch(url="https://android.googleapis.com/gcm/send",
                                payload=json.dumps(bodyFields),
                                method=urlfetch.POST,
                                headers={'Content-Type': 'application/json', 'Authorization': 'key=' + globals.GOOGLE_CLOUD_MESSAGING_API})

        result_content = result.content

        if result_content is not None:
            logging.info("send_google_cloud_message; " + str(result_content))

        return True
