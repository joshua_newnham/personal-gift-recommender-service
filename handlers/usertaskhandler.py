__author__ = 'jnewnham'

from basetaskhandler import BaseTaskHandler
from model import *
from google.appengine.api import urlfetch
from google.appengine.api import memcache
from google.appengine.api.labs import taskqueue
import json
import logging
import globals


class ProcessUserTaskHandler(BaseTaskHandler):
    def post(self):
        logging.info("ProcessUserTaskHandler.post")

        user_key = self.request.get("user_key")
        task_cache_id = self.request.get("task_cache_id", default_value=None)
        gcm_reg_id = self.request.get("gcm_reg_id", default_value=None)

        if task_cache_id is None or user_key is None:
            logging.warning("Exiting ProcessUserTaskHandler - task_cache_id and/or user_key")
            return

        memcache.set("processing-" + user_key, "processing", time=3000)

        user = User.getUserWithKey(user_key)

        contacts = self.getContactsFromTaskCache(user, task_cache_id)

        if contacts is not None and len(contacts) > 0:
            self.findUserMetaData(contacts, gcm_reg_id)

        memcache.delete("processing-" + user_key)

        # notify user that the task has finished
        if gcm_reg_id is not None:
            logging.info("ProcessUserTaskHandler.send_google_cloud_message: " + gcm_reg_id)
            self.send_google_cloud_message(gcm_reg_id, "adding_contacts_finished")


    def getContactsFromTaskCache(self, user, task_cache_id):
        logging.info("ProcessUserTaskHandler.getContactsFromTaskCache")
        contacts = []
        json_items = None

        taskCache = TaskCache.getTaskCacheById(task_cache_id)
        if taskCache:
            json_items = json.loads(taskCache.payload)
            db.delete(taskCache)

        if user is None:
            logging.warning("ProcessUserTaskHandler.getContactsFromTaskCache; no user")
            return contacts

        contacts.append(user)

        if json_items is None:
            logging.warning("ProcessUserTaskHandler.getContactsFromTaskCache; no items for task_cache_id " + task_cache_id)
            return contacts

        for json_item in json_items:
            is_new = False
            if "emails" in json_item:
                emails = json_item["emails"]
                contact = User.getUserWithEmailFrom(emails)

                if contact is None:
                    contact = User()
                    contact.emails = emails
                    is_new = True

            if "name" in json_item:
                contact.name = json_item["name"]

            contact.put()

            if is_new or user.getContactWithKey(contact.key()) is None:
                userContacts = UserContacts(user=user, contact=contact)
                userContacts.put()

            contacts.append(contact)

        return contacts

    def findUserMetaData(self, contacts, gcm_reg_id):
        self.add_bg_task(taskqueue.Task(name="analyseusertask-" + datetime.now().time().strftime("%H%M%S"),
                                        url=globals.TASK_URL_ANALYSE_USER,
                                        params={"gcm_reg_id": gcm_reg_id}))