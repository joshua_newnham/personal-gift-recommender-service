__author__ = 'jnewnham'

from basehandler import BaseHandler
from model import *
import webapp2
import json
import logging
from google.appengine.api.labs import taskqueue
import globals


class UserHandler(BaseHandler):
    def get(self):
        self.post()

    def post(self):
        logging.debug("UserHandler")

        user = self.get_user_from_request()

        if user is None:
            self.respond_with_error("unable to find associated to user", globals.STATUS_ERROR)
            return

        # get all the users contacts
        contacts = []
        all_contacts = user.getContacts()

        if all_contacts is not None and len(all_contacts) > 0:
            for contact in all_contacts:
                contacts.append(contact.toDict())

        res = {"status": "success", "message": "contacts", "user": user.toDict(), "contacts": contacts, "status_code": globals.STATUS_OK}

        outputAsJson = json.dumps(res)

        self.response.headers.add_header('Content-Type', 'application/json')
        self.response.write(outputAsJson)


class UserUpdateHandler(BaseHandler):

    def post(self):
        logging.debug("UserUpdateHandler")

        user = None
        user_key = None

        user_key = self.request.get("user_key", default_value=None)

        postData = self.request.body

        if postData is None:
            logging.error("missing postData")
            self.respond_with_error("missing parameters", globals.STATUS_ERROR)
            return

        if user_key is not None:
            user = User.getUserWithKey(user_key)

        json_obj = json.loads(postData)

        if "user" in json_obj:
            user_obj = json_obj["user"]
            name = ""
            emails = []

            if "name" in user_obj:
                name = user_obj["name"]

            if "remote_id" in user_obj:
                remote_id = user_obj["remote_id"]

                if user is None and remote_id is not None and len(remote_id) > 0:
                    user = User.getUserWithKey(remote_id)

            if user is None:
                if "emails" in user_obj:
                    emails = user_obj["emails"]

                    user = User.getUserWithEmailFrom(emails)

            if user is None:
                user = User(name=name, emails=emails)
                user.put()

                user_key = str(user.key())

        if user is None:
            logging.error("no user found")
            self.respond_with_error("no user found", globals.STATUS_ERROR)
            return

        logging.debug("UserUpdateHandler.user_key " + user_key)

        if "account_tokens" in json_obj:
            account_tokens = json_obj["account_tokens"]
            for new_account_token in account_tokens:
                account = new_account_token["account"]
                token = new_account_token["token"]

                account_token = user.getAccountTokenForAccount(account)
                if account_token is None:
                    account_token = UserAccountToken(user=user, account=account, token=token)
                    account_token.put()
                else:
                    account_token.token = token
                    account_token.put()

        device_id = None
        gcm_reg_id = None

        if "device_id" in json_obj:
            device_id = json_obj["device_id"]

        if "gcm_reg_id" in json_obj:
            gcm_reg_id = json_obj["gcm_reg_id"]

        if device_id is not None:
            user_device = user.getDeviceWithDeviceId(device_id)

            if user_device is None:
                user_deivce = UserDevice(user=user, device_id=device_id, gcm_reg_id=gcm_reg_id)
                user_deivce.put()
            elif gcm_reg_id is not None:
                user_device.gcm_reg_id = gcm_reg_id
                user_device.put()

        if "contacts" in json_obj:
            contacts_obj = json_obj["contacts"]
            contacts_obj_str = json.dumps(contacts_obj, ensure_ascii=False)

            taskCacheId = "contacts-" + str(user.key())
            taskCache = TaskCache(taskCacheId=taskCacheId, payload=contacts_obj_str)
            taskCache.put()

            self.add_bg_task(taskqueue.Task(name="processusertask-" + datetime.now().time().strftime("%H%M%S"),
                                            url=globals.TASK_URL_PROCESS_USER,
                                            params={'user_key': user_key, "task_cache_id": taskCacheId, "gcm_reg_id": gcm_reg_id}))

        res = {"status": "success", "message": "user updating", "user": user.toDict(), "status_code": globals.STATUS_OK}

        outputAsJson = json.dumps(res)

        self.response.headers.add_header('Content-Type', 'application/json')
        self.response.write(outputAsJson)


class UserRecommendedProductsHandler(BaseHandler):
    def get(self):
        self.post()

    def post(self):
        logging.debug("UserRecommendedProductsHandler")

        postData = self.request.body
        json_obj = json.loads(postData)

        user_obj = None

        if "user" in json_obj:
            user_obj = json_obj["user"]

        user = self.get_user_from_josn_object(user_obj)

        if user is None:
            logging.warning("UserRecommendedProductsHandler.no user found")
            self.respond_with_error("unable to find user", globals.STATUS_ERROR)
            return

        contacts = []
        if "contacts" in json_obj:
            contacts_obj = json_obj["contacts"]
            for contact_obj in contacts_obj:
                contact = self.get_user_from_josn_object(contact_obj)
                if contact is not None:
                    contacts.append(contact)

        if len(contacts) == 0:
            logging.warning("UserRecommendedProductsHandler.no contacts found")
            self.respond_with_error("no contacts", globals.STATUS_ERROR)
            return

        products = self.get_recommended_products_for_users(contacts)

        products_as_dict = []
        if products is not None:
            for product in products:
                products_as_dict.append(product.toDict())

        res = {"status": "success", "message": "user updating", "user": user.toDict(), "products": products_as_dict, "status_code": globals.STATUS_OK}

        outputAsJson = json.dumps(res)

        self.response.headers.add_header('Content-Type', 'application/json')
        self.response.write(outputAsJson)


class UserUpdateStatusHandler(BaseHandler):
    def post(self):
        user_key = self.request.get("user_key", default_value=None)


class UserAddProductsHandler(BaseHandler):
    def post(self):
        user_key = self.request.get("user_key", default_value=None)

        pass