__author__ = 'jnewnham'

import webapp2
from google.appengine.api.labs import taskqueue
from datetime import datetime, time
from urllib import urlencode
import json
from google.appengine.api import urlfetch
import globals
import logging
from model import *
import random

class BaseHandler(webapp2.RequestHandler):

    def __init__(self, request=None, response=None):
        self.initialize(request, response)
        self.current_milli_time = lambda: int(round(time.time() * 1000))


    def get_user_from_request(self):
        user = None

        user_key = self.request.get("user_key", default_value=None)
        if user_key is not None:
            user = User.getUserWithKey(user_key)

        if user is None:
            user_email = self.request.get("email", default_value=None)

            if user_email is not None:
                user = User.getUserWithEmail(user_email)

        if user is None:
            user_name = self.request.get("name", default_value=None)

            if user_name is not None:
                user = User.getUserWithName(user_name)

        return user

    def get_user_from_josn_object(self, user_obj):

        if user_obj is None:
            return None

        if "remote_id" in user_obj:
            remote_id = user_obj["remote_id"]

            if remote_id is not None and len(remote_id) > 0:
                user = User.getUserWithKey(remote_id)
                if user is not None:
                    return user

        if "emails" in user_obj:
            emails = user_obj["emails"]
            if emails is not None and len(emails) > 0:
                user = User.getUserWithEmailFrom(emails)
                if user is not None:
                    return user

        if "name" in user_obj:
            user_name = user_obj["name"]
            if user_name is not None and len(user_name) > 0:
                user = User.getUserWithName(user_name)
                if user is not None:
                    return user

        return None

    def respond_with_error(self, msg, status_code):
        res = {"status": "failed", "message": msg, "status_code": status_code}
        outputAsJson = json.dumps(res)

        self.response.headers.add_header('Content-Type', 'application/json')
        self.response.write(outputAsJson)

    def get_bg_task_queue(self):
        return taskqueue.Queue("taskqueue")

    def add_bg_task(self, task):
        self.get_bg_task_queue().add(task)

    def getCurrentTimeInMS(self):
        return int(time.time() * 1000)

    def getCurrentTimeInSecs(self):
        #return time.time()
        delta = datetime.now() - datetime(1970, 1, 1)
        return delta.total_seconds()

    def alchemy_api(self, url, query, type='text'):
        params = {}
        params['apikey'] = globals.ALCHEMYAPI_API_KEY
        params['outputMode'] = 'json'
        params[type] = query

        try:
            post_url = url + '?' + urlencode(params).encode('utf-8')
        except UnicodeEncodeError:
            return # ignore

        #logging.info("BaseHandler.alchemy_api.posting " + post_url)

        fetch_response = urlfetch.fetch(post_url)

        #if fetch_response.status_code == 200:
        if fetch_response.content is not None:
            return json.loads(fetch_response.content)

        return None

    def get_recommended_products_for_users(self, users):
        logging.info("get_recommended_products_for_user")

        products = []
        all_products = Product.getAll()
        products_len = len(all_products)

        if len(all_products) > 0:
            counter = 0
            while len(products) < 20 and counter < 1000:
                counter = counter + 1
                index = random.randint(0, products_len-1)
                product = all_products[index]
                if product not in products:
                    products.append(product)

        return products

