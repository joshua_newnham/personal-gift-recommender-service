__author__ = 'jnewnham'

import csv
import webapp2
from google.appengine.ext import blobstore
from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.ext import db
from handlers import *
from model import *
from google.appengine.api import urlfetch
import re
import urllib
import urllib2
from bs4 import *
import logging
#try:
#    import xml.etree.cElementTree as ET
#except ImportError:
#    import xml.etree.ElementTree as ET

class CSVUploadHandler(BaseHandler):
    def get(self):
        logging.debug("CSVUploadHandler")
        upload_url = blobstore.create_upload_url('/tasks/csvuploader')

        html_string = """
         <form action="%s" method="POST" enctype="multipart/form-data">
        Upload File:
        <input type="file" name="file"> <br>
        <input type="submit" name="submit" value="Submit">
        </form>""" % upload_url

        self.response.write(html_string)


class CSVUploaderHandler(blobstore_handlers.BlobstoreUploadHandler):
    def post(self):
        logging.debug("CSVUploaderHandler")
        upload_files = self.get_uploads('file')  # 'file' is file upload field in the form
        blob_info = upload_files[0]
        self.process_csv(blob_info)

        blobstore.delete(blob_info.key())  # optional: delete file after import

        self.add_bg_task(taskqueue.Task(name="processskuitemstask-" + datetime.now().time().strftime("%H%M%S"),
                                        url=globals.TASK_URL_PROCESS_SKU_ITEMS))

        #self.redirect("/")
        html_string = """<html><body><p>file uploading</p></body></html>"""
        self.response.write(html_string)

    def process_csv(self, blob_info):
        blob_reader = blobstore.BlobReader(blob_info.key())
        # o = open(mypath,'rU')
        #reader = csv.reader(blob_reader, delimiter=',')
        reader = BlobIterator(blob_reader)
        for row in reader:
            sku = row.split()[0]
            #logging.info(str(sku))
            productSKU = ProductSKU(sku=sku)
            productSKU.put()

    def get_bg_task_queue(self):
        return taskqueue.Queue("taskqueue")

    def add_bg_task(self, task):
        self.get_bg_task_queue().add(task)


class ProcessProductSKUsTaskHandler(BaseTaskHandler):
    def get(self):
        self.post()

    def post(self):
        logging.debug("ProcessProductSKUsTaskHandler")
        #query = ProductSKU.all()
        productsSkus = ProductSKU.getAll()

        #logging.debug("ProcessProductSKUsTaskHandler " + str(query.count()))

        #for item in query.run():
        for item in productsSkus:
            self.download_product(item)
            item.delete()

    def download_product(self, productSKU):
        #logging.debug("download_product")
        sku = productSKU.sku
        if sku is None or len(sku) == 0:
            return None

        refresh_product = True # TODO implement expiry logic
        is_new_product = False

        product = Product.getProductBySku(sku)

        if product is None or refresh_product:
            product = self.fetchArgosProduct(sku)
            is_new_product = True

        if product is None:
            return None

        if is_new_product or product.tags is None or product.tags.count() == 0:
            self.fetchProductMetaData(product)

        return product


    def fetchProductMetaData(self, product):
        #logging.debug("fetchProductMetaData")
        if product is None:
            return

        if product.short_description is None and product.long_description is None:
            return

        if product.short_description is not None:
            taxonomy_json = self.alchemy_api(globals.ALCHEMYAPI_TAXONOMY_TEXT, product.short_description, "text")
        else:
            taxonomy_json = self.alchemy_api(globals.ALCHEMYAPI_TAXONOMY_TEXT, product.long_description, "text")

        #logging.info(json.dumps(taxonomy_json))

        if taxonomy_json is not None:
            if "taxonomy" in taxonomy_json:
                taxonomy_item_json = None
                if len(taxonomy_json["taxonomy"]) > 0:
                    taxonomy_item_json = taxonomy_json["taxonomy"][0]

                #for taxonomy_item_json in taxonomy_json["taxonomy"]:
                if taxonomy_item_json is not None:
                    score = 0.0
                    if "score" in taxonomy_item_json:
                        score_as_string = taxonomy_item_json["score"]

                        try:
                            score = float(score_as_string)
                        except ValueError:
                            pass

                    if score <= 0.7:
                        return

                    label = taxonomy_item_json["label"]

                    if label is not None and len(label) > 0:
                        root_tag = None
                        parent_tag = None
                        taxonomys = label.split('/')
                        for taxonomy in taxonomys:
                            taxonomy = taxonomy.strip()
                            if len(taxonomy) == 0:
                                continue

                            tag = Tag.getTagWithTag(taxonomy)

                            if tag is None:
                                tag = Tag(tag=taxonomy)
                                if parent_tag is not None:
                                    tag.parent = parent_tag
                                tag.put()
                            elif tag.parent_tag is None:
                                tag.parent_tag = parent_tag
                                tag.put()

                            parent_tag = tag

                            if root_tag is None:
                                root_tag = tag

                        # only assign the root tag to the product
                        if tag is not None:
                            product_tag = product.getProductTagWithTagKey(tag.key())

                            if product_tag is not None:
                                #product_tag.counter = product_tag.counter + 1
                                #product_tag.put()
                                pass
                            else:
                                product_tag = ProductTag(product=product, tag=tag)
                                product_tag.put()


    def fetchArgosProduct(self, productId):
        #logging.debug("fetchArgosProduct")
        url = globals.ARGOS_PRODUCT_URL + "/" + productId

        params = {
            "apiKey": globals.ARGOS_API_KEY
        }

        headers = {
            'Accept': 'application/vnd.homeretailgroup.product;version=1;format=json',
            'format': "json",
            'Content-Language': 'en-gb',
            'X-Original-User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/535.19',
            'httpMethod': 'GET'
        }

        response_obj = urlfetch.fetch(url=url + '?' + urllib.urlencode(params), headers=headers)

        if response_obj is None or response_obj.status_code != 200 or response_obj.content is None:
            return None

        product = Product.getProductBySku(productId)

        if product is None:
            product = Product(sku=productId)

        #logging.info(str(response_obj.content))

        soup = BeautifulSoup(response_obj.content)
        elem_brand = soup.find({'prd:brand': re.compile("^description$", re.I)})
        if elem_brand:
            product.brand = elem_brand.text
        elem_sd = soup.find({'prd:shortdescription': re.compile("^description$", re.I)})
        if elem_sd:
            product.short_description = elem_sd.text
        elem_ld = soup.find({'prd:longdescription': re.compile("^description$", re.I)})
        if elem_ld:
            product.long_description = elem_ld.text

        for price_information in soup.findAll({'prd:pricinginformation': re.compile("^description$", re.I)}):
            elem_current_price = price_information.find({'prd:currentprice': re.compile("^description$", re.I)})
            if elem_current_price:
                elem_price = elem_current_price.find({'prd:price': re.compile("^description$", re.I)})
                if elem_price.attrs:
                    attrs = dict(elem_price.attrs)
                    if attrs['currency'] == "GBP":
                        price = str(elem_price.text.encode('utf8'))
                        try:
                            product.price = float(price)
                        except ValueError:
                            product.price = 0.0

                        break
                else:
                    price = str(elem_price.text.encode('utf8'))
                    try:
                        product.price = float(price)
                    except ValueError:
                        product.price = 0.0

        #product["average_rating"] = soup.find({'prd:averagerating': re.compile("^description$", re.I)}).text
        #product["number_of_reviews"] = soup.find({'prd:numberreviews': re.compile("^description$", re.I)}).text

        backup_image_url = None

        for media in soup.find({'prd:associatedmedia': re.compile("^description$", re.I)}):
            #logging.info("media " + str(media))
            if media['usage'] == "largeImage":
                product.image_url = str(media['href'])
            elif backup_image_url is None:
                backup_image_url = str(media["href"])

        if (product.image_url is None or len(product.image_url) == 0) and backup_image_url is not None and len(backup_image_url) > 0:
            product.image_url = backup_image_url

        #logging.info("image url " + str(product.image_url))

        product.put()

        return product


# http://d4nt.com/parsing-large-csv-blobs-on-google-app-engine/
class BlobIterator:
    """Because the python csv module doesn't like strange newline chars and
    the google blob reader cannot be told to open in universal mode, then
    we need to read blocks of the blob and 'fix' the newlines as we go"""

    def __init__(self, blob_reader):
        self.blob_reader = blob_reader
        self.last_line = ""
        self.line_num = 0
        self.lines = []
        self.buffer = None

    def __iter__(self):
        return self

    def next(self):
        if not self.buffer or len(self.lines) == self.line_num:
            self.buffer = self.blob_reader.read(1048576) # 1MB buffer
            self.lines = self.buffer.splitlines()
            self.line_num = 0

            # Handle special case where our block just happens to end on a new line
            if self.buffer[-1:] == "\n" or self.buffer[-1:] == "\r":
                self.lines.append("")

        if not self.buffer:
            raise StopIteration

        if self.line_num == 0 and len(self.last_line) > 0:
            result = self.last_line + self.lines[self.line_num] + "\n"
        else:
            result = self.lines[self.line_num] + "\n"

        self.last_line = self.lines[self.line_num]
        self.line_num += 1

        return result