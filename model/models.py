__author__ = 'jnewnham'

import json
from google.appengine.ext import db
from datetime import *
from utils import *


class BaseModel(db.Model):
    created_on = db.DateTimeProperty(auto_now=True)


class TaskCache(BaseModel):
    taskCacheId = db.StringProperty(default=None)
    payload = db.TextProperty(default=None)

    @staticmethod
    def getTaskCacheById(taskCacheId):
        query = db.GqlQuery("SELECT * FROM TaskCache WHERE taskCacheId = :1", taskCacheId)

        if query.count() > 0:
            return query.get()

        else:
            return None


class User(BaseModel):
    name = db.StringProperty(default="")
    first_name = db.StringProperty(default="")
    last_name = db.StringProperty(default="")
    emails = db.StringListProperty()
    gender = db.IntegerProperty(default=-1)
    analysis_timestamp = db.FloatProperty(default=0.0)
    fb_id = db.StringProperty(default="")
    twitter_handle = db.StringProperty(default="")
    linkedin_id = db.StringProperty(default="")

    def getContactWithKey(self, contacts_key):
        if self.contacts and self.contacts.count() > 0:
            for userContact in self.contacts:
                if userContact.contact.key() == contacts_key:
                    return userContact.contact

        return None

    def getContacts(self):
        contactsArray = []

        if self.contacts and self.contacts.count() > 0:
            for userContact in self.contacts:
                contactsArray.append(userContact.contact)

        return contactsArray

    def getAccountTokens(self):
        array = []

        if self.account_tokens and self.account_tokens.count() > 0:
            for account_token in self.account_tokens:
                array.append(account_token)

        return array

    def getAccountTokenForAccount(self, account):
        if self.account_tokens and self.account_tokens.count() > 0:
            for account_token in self.account_tokens:
                if account_token.account == account:
                    return account_token

        return None

    def getProducts(self):
        array = []

        if self.products and self.products.count() > 0:
            for userProduct in self.products:
                array.append(userProduct.product)

        return array

    def getDevices(self):
        array = []

        if self.devices and self.devices.count() > 0:
            for device in self.devices:
                array.append(device)

        return array

    def getDeviceWithDeviceId(self, device_id):
        if self.devices and self.devices.count() > 0:
            for device in self.devices:
                if device.device_id == device_id:
                    return device

        return None

    def toDict(self):
        return {"remote_id": str(self.key()),
                "name": self.name,
                "first_name": self.first_name,
                "last_name": self.last_name,
                "emails": self.emails,
                "gender": self.gender,
                "fb_id": self.fb_id,
                "twitter_handle": self.twitter_handle,
                "linkedin_id": self.linkedin_id,
                "created_on": str(self.created_on)}

    def toJSON(self):
        output = self.toDict()
        return json.dumps(output)

    @staticmethod
    def getUserWithKey(userKey):
        if userKey is None:
            return None

        query = db.GqlQuery("SELECT * FROM User WHERE __key__ = KEY('" + userKey + "')")

        if query.count() > 0:
            return query.get()

        else:
            return None

    @staticmethod
    def getUserWithEmail(email):
        if email is None:
            return None

        query = db.GqlQuery("SELECT * FROM User WHERE emails = :1", email)

        if query.count() > 0:
            return query.get()

        else:
            return None

    @staticmethod
    def getUserWithName(name):
        if name is None:
            return None

        all_users = User.getAll()

        for user in all_users:
            if textutils.nltk_fuzzy_match(name, user.name):
                return user

        return None

    @staticmethod
    def getUserWithEmailFrom(emails):
        if emails is None:
            return None

        for email in emails:
            query = db.GqlQuery("SELECT * FROM User WHERE emails = :1", email)

            if query.count() > 0:
                return query.get()


        return None

    @staticmethod
    def count():
        return User.all(keys_only=True).count()

    @staticmethod
    def getAll():
        users = []
        query = User.all()

        for user in query.run():
            users.append(user)

        return users


class UserContacts(BaseModel):
    user = db.ReferenceProperty(User, collection_name='contacts')
    contact = db.ReferenceProperty(User)
    user_relationship = db.IntegerProperty(default=0)
    times_contacted = db.IntegerProperty(default=0)


class UserAccountToken(BaseModel):
    user = db.ReferenceProperty(User, collection_name='account_tokens')
    account = db.StringProperty(default="")
    token = db.StringProperty(default="")

    @staticmethod
    def getAll():
        items = []
        query = UserAccountToken.all()

        for item in query.run():
            items.append(item)

        return items


class Product(BaseModel):
    sku = db.StringProperty(default="")
    brand = db.StringProperty(default="")
    image_url = db.StringProperty(default="")
    title = db.StringProperty(default="")
    price = db.FloatProperty(default=0.0)
    short_description = db.StringProperty(default="")
    long_description = db.TextProperty(default="")

    def getProductTagWithTagKey(self, product_tag_key):
        for product_tag in self.tags:
            if product_tag.tag.key() == product_tag_key:
                return product_tag

        return None

    def toDict(self):
        tags_array = []

        for product_tag in self.tags:
            tags_array.append(product_tag.tag.toDict())

        return {"sku": self.sku,
                "image_url": self.image_url,
                "title": self.title,
                "brand": self.brand,
                "price": str(self.price),
                "short_description": self.short_description,
                "long_description": self.long_description,
                "tags": tags_array,
                "created_on": str(self.created_on)}

    def toJSON(self):
        output = self.toDict()
        return json.dumps(output)

    @staticmethod
    def getProductBySku(sku):
        if sku is None:
            return None

        query = db.GqlQuery("SELECT * FROM Product WHERE sku = :1", sku)

        if query.count() > 0:
            return query.get()

        else:
            return None

    @staticmethod
    def count():
        return Product.all(keys_only=True).count()

    @staticmethod
    def getAll():
        items = []
        query = Product.all()

        for item in query.run():
            items.append(item)

        return items


class Tag(BaseModel):
    tag = db.StringProperty(default="")
    parent_tag = db.SelfReferenceProperty(collection_name='children')

    def toDict(self):

        if self.parent_tag is None:
            return {"tag": self.tag,
                    "created_on": str(self.created_on)}

        tag_tree = []

        # find the parent
        tag_tree.append(self)
        next_parent_tag = self.parent_tag
        while next_parent_tag is not None:
            tag_tree.append(next_parent_tag)
            next_parent_tag = next_parent_tag.parent_tag

        top_node = None
        current_node = None
        while len(tag_tree) > 0:
            current_tag = tag_tree.pop()
            if current_node is None:
                current_node = {"tag": current_tag.tag, "created_on": str(current_tag.created_on)}
                top_node = current_node
            else:
                current_node["children"] = {"tag": current_tag.tag, "created_on": str(current_tag.created_on)}
                current_node = current_node["children"]

        return top_node


    def toJSON(self):
        output = self.toDict()
        return json.dumps(output)


    @staticmethod
    def getTagWithTag(tag):
        if tag is None:
            return None

        query = db.GqlQuery("SELECT * FROM Tag WHERE tag = :1", tag)

        if query.count() > 0:
            return query.get()

        else:
            return None


class ProductTag(BaseModel):
    product = db.ReferenceProperty(Product, collection_name='tags')
    tag = db.ReferenceProperty(Tag)
    counter = db.IntegerProperty(default=1)


class UserProducts(BaseModel):
    user = db.ReferenceProperty(User, collection_name='products')
    product = db.ReferenceProperty(Product, collection_name='users')


class UserDevice(BaseModel):
    user = db.ReferenceProperty(User, collection_name='devices')
    device_id = db.StringProperty(default="")
    gcm_reg_id = db.StringProperty(default="")


class ProductSKU(BaseModel):
    sku = db.StringProperty()

    @staticmethod
    def getAll():
        items = []
        query = ProductSKU.all()

        for item in query.run():
            items.append(item)

        return items
