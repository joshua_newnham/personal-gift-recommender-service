__author__ = 'jnewnham'

TASK_URL_PROCESS_USER       =   "/tasks/processuser"
TASK_URL_ANALYSE_USER       =   "/tasks/analyseusers"
TASK_URL_PROCESS_SKU_ITEMS  =   "/tasks/processskuitems"

ACCOUNT_TWITTER         = "com.twitter.android.auth.login"
ACCOUNT_FACEBOOK        = "com.facebook.auth.login"
ACCOUNT_LINKEDIN        = "com.linkedin.android"

USER_ANALYSIS_REFRESH   = 2073600

STATUS_ERROR            = -1
STATUS_OK               = 1

ARGOS_API_KEY = "846bpn8kfnrs47qjbr8w45ea"
ARGOS_PRODUCT_URL = "https://api.homeretailgroup.com/product/argos"
ARGOS_SEARCH_URL = "https://api.homeretailgroup.com/search/argos/search"
ARGOS_CATEGORY_SEARCH_URL = "https://api.homeretailgroup.com/search/argos/category"
#ARGOS_PRODUCT_SEARCH_URL = "https://api.homeretailgroup.com/search/argos/product"
ARGOS_PRODUCT_SEARCH_URL = "https://api.homeretailgroup.com/search/argos"

ALCHEMYAPI_BASE_URL = 'http://access.alchemyapi.com/calls'
ALCHEMYAPI_API_KEY = "160674cf4b838ce76654a833fa0dbe09ff9a096b"
ALCHEMYAPI_TAXONOMY_TEXT = ALCHEMYAPI_BASE_URL + "/text/TextGetRankedTaxonomy"
ALCHEMYAPI_TAXONOMY_HTML = ALCHEMYAPI_BASE_URL + "/html/HTMLGetRankedTaxonomy"

GOOGLE_CLOUD_MESSAGING_API = "AIzaSyBqXDEyx0CFd5oSqjSZ7sqLqF2Ry2GQL8o"
