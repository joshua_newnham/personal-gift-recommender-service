import urllib
import urllib2
import json
import time
import re
from datetime import *
from alchemyapi.alchemyapi import AlchemyAPI
from urllib import urlencode
import requests

API_KEY = "846bpn8kfnrs47qjbr8w45ea"
OAA = "846bpn8kfnrs47qjbr8w45ea" # open access assessment
ARGOS_PRODUCT_URL = "https://api.homeretailgroup.com/product/argos"
ARGOS_SEARCH_URL = "https://api.homeretailgroup.com/search/argos/search"
ARGOS_CATEGORY_SEARCH_URL = "https://api.homeretailgroup.com/search/argos/category"
#ARGOS_PRODUCT_SEARCH_URL = "https://api.homeretailgroup.com/search/argos/product"
ARGOS_PRODUCT_SEARCH_URL = "https://api.homeretailgroup.com/search/argos"

def argosProductLookup(apikey, productId):
    url = ARGOS_PRODUCT_URL + "/" + productId
    response = None

    params = {
        "apiKey" : API_KEY,
    }

    headers = {
        'Accept':'application/vnd.homeretailgroup.product;version=1;format=json',
        'format' : "json",
        'Content-Language':'en-gb',
        'X-Original-User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/535.19',
        'httpMethod':'GET'
    }

    request_object = urllib2.Request(url + '?' + urllib.urlencode(params), None, headers)

    print "Calling " + request_object.get_full_url()

    #request_object.add_header('Accept', 'application/vnd.homeretailgroup.product;version=1;format=xml')
    #request_object.add_header('X-Original-User-Agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/535.19')
    #request_object.add_header("Accept-Encoding", "gzip,deflate")
    #request_object.add_header("Content-Language", "en-gb")

    try:
        response = urllib2.urlopen(request_object)
    except urllib2.HTTPError as e:
        print e

    if response is not None:
        return response.read()

    return ""

def argosSearch(apikey, query):
    url = ARGOS_PRODUCT_SEARCH_URL
    response = None

    params = {
        "query" : query,
        "apiKey" : API_KEY,
    }

    headers = {
        'Accept':'application/vnd.homeretailgroup.search;version=1;format=json',
        'format' : "json",
        'Content-Language':'en-gb',
        'X-Original-User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/535.19',
        'httpMethod':'GET'
    }

    request_object = urllib2.Request(url + '?' + urllib.urlencode(params), None, headers)

    print "Calling " + request_object.get_full_url()

    #request_object.add_header('Accept', 'application/vnd.homeretailgroup.product;version=1;format=xml')
    #request_object.add_header('X-Original-User-Agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/535.19')
    #request_object.add_header("Accept-Encoding", "gzip,deflate")
    #request_object.add_header("Content-Language", "en-gb")

    try:
        response = urllib2.urlopen(request_object)
    except urllib2.HTTPError as e:
        print e

    if response is not None:
        print response.read()

from bs4 import *

soup = BeautifulSoup(argosProductLookup(API_KEY, "4436405"))
print soup

brand_elem = soup.find({'prd:brand': re.compile("^description$", re.I)})
if brand_elem:
    print brand_elem.text

short_description = None
short_description_elem = soup.find({'prd:shortdescription': re.compile("^description$", re.I)})
if short_description_elem:
    short_description = short_description_elem.text
    print short_description_elem.text

long_description_elem = soup.find({'prd:longdescription': re.compile("^description$", re.I)})
if long_description_elem:
    print long_description_elem.text

for price_information in soup.findAll({'prd:pricinginformation': re.compile("^description$", re.I)}):
    if price_information.find({'prd:currentprice': re.compile("^description$", re.I)}):
        attrs = dict(soup.find({'prd:currentprice': re.compile("^description$", re.I)}).find({'prd:price': re.compile("^description$", re.I)}).attrs) #
        if attrs['currency'] == "GBP":
            price = str(soup.find({'prd:currentprice': re.compile("^description$", re.I)}).find({'prd:price': re.compile("^description$", re.I)}).text.encode('utf8'))
            print "price " + price
            print float(price)

#print soup.find({'prd:averagerating': re.compile("^description$", re.I)}).text
#print soup.find({'prd:numberreviews': re.compile("^description$", re.I)}).text

for media in soup.find({'prd:associatedmedia': re.compile("^description$", re.I)}):
    attrs = dict(media.attrs)
    if "usage" in media and "href" in media and media['usage'] == "largeImage":
        print media['href']


def alchemy_api(url, query, type='text'):
    params = {}
    params['apikey'] = "160674cf4b838ce76654a833fa0dbe09ff9a096b"
    params['outputMode'] = 'json'
    params[type] = query

    post_url = "http://access.alchemyapi.com/calls" + url + '?' + urlencode(params).encode('utf-8')

    print post_url

    return requests.get(post_url).json()

if short_description is not None:
    json_obj = alchemy_api("/text/TextGetRankedTaxonomy", short_description, "text")
    if "taxonomy" in json_obj and len(json_obj["taxonomy"]) > 0:
         taxonomy = json_obj["taxonomy"][0]["label"].split('/')
         print taxonomy[1].strip()

    print json.dumps(json_obj, indent=1)
#print json.dumps(alchemy_api("/text/TextGetRankedTaxonomy", longt_description, "text"), indent=1)


#argosSearch(API_KEY, "tv")
#argosProductLookup(API_KEY, "8615710")
#alchemyapi = AlchemyAPI()
#print alchemyapi.category('text', 'Access a world of entertainment with this Sony 42 inch Full HD 1080p TV. Providing extensive connectivity options including Wi-Fi compatibility, this Smart TV also features Dolby sound system to further enhance your viewing experience. Integrated Freeview HD digital tuner gives you the pick of the channels, whilst the 14-day EPG allows you to plan your TV viewing. PC input through HDMI socket (PC requires HDMI socket as well).Top 5 Smart apps include: Music Unlimited, BBC iPlayer, LOVEFiLM, Netflix, Sony Entertainment Television.')
#result = json.loads(alchemyapi.taxonomy('text', 'Access a world of entertainment with this Sony 42 inch Full HD 1080p TV. Providing extensive connectivity options including Wi-Fi compatibility, this Smart TV also features Dolby sound system to further enhance your viewing experience. Integrated Freeview HD digital tuner gives you the pick of the channels, whilst the 14-day EPG allows you to plan your TV viewing. PC input through HDMI socket (PC requires HDMI socket as well).Top 5 Smart apps include: Music Unlimited, BBC iPlayer, LOVEFiLM, Netflix, Sony Entertainment Television.'))
#print alchemyapi.keywords('text', 'Access a world of entertainment with this Sony 42 inch Full HD 1080p TV. Providing extensive connectivity options including Wi-Fi compatibility, this Smart TV also features Dolby sound system to further enhance your viewing experience. Integrated Freeview HD digital tuner gives you the pick of the channels, whilst the 14-day EPG allows you to plan your TV viewing. PC input through HDMI socket (PC requires HDMI socket as well).Top 5 Smart apps include: Music Unlimited, BBC iPlayer, LOVEFiLM, Netflix, Sony Entertainment Television.')
#print alchemyapi.relations('text', 'Access a world of entertainment with this Sony 42 inch Full HD 1080p TV. Providing extensive connectivity options including Wi-Fi compatibility, this Smart TV also features Dolby sound system to further enhance your viewing experience. Integrated Freeview HD digital tuner gives you the pick of the channels, whilst the 14-day EPG allows you to plan your TV viewing. PC input through HDMI socket (PC requires HDMI socket as well).Top 5 Smart apps include: Music Unlimited, BBC iPlayer, LOVEFiLM, Netflix, Sony Entertainment Television. Cost 299')
#print alchemyapi.concepts('text', 'Access a world of entertainment with this Sony 42 inch Full HD 1080p TV. Providing extensive connectivity options including Wi-Fi compatibility, this Smart TV also features Dolby sound system to further enhance your viewing experience. Integrated Freeview HD digital tuner gives you the pick of the channels, whilst the 14-day EPG allows you to plan your TV viewing. PC input through HDMI socket (PC requires HDMI socket as well).Top 5 Smart apps include: Music Unlimited, BBC iPlayer, LOVEFiLM, Netflix, Sony Entertainment Television.')
#result = alchemyapi.keywords('text', 'Head of Mobile @Razorfish, London. Love beautiful code, ubicomping, user centric design, making things smart. Husband to @daniellenewnham + dad to little Benji')
#result = alchemyapi.keywords('text', 'Head of Mobile @Razorfish, London. Love beautiful code, ubicomping, user centric design, making things smart. Husband to @daniellenewnham + dad to little Benji')
#print alchemyapi.entities('text', 'Head of Mobile @Razorfish, London. Love beautiful code, ubicomping, user centric design, making things smart. Husband to @daniellenewnham + dad to little Benji')
#print alchemyapi.relations('text', 'Head of Mobile @Razorfish, London. Love beautiful code, ubicomping, user centric design, making things smart. Husband to @daniellenewnham + dad to little Benji')
#print alchemyapi.keywords("url", "http://www.argos.co.uk/static/Product/partNumber/1023828.htm")
#print alchemyapi.relations("url", "http://www.argos.co.uk/static/Product/partNumber/1023828.htm")
#print alchemyapi.entities("url", "http://www.argos.co.uk/static/Product/partNumber/1023828.htm")
#print alchemyapi.taxonomy("url", "http://www.heatworld.com/Entertainment/Films/2014/06/Tom-Cruise-is-lining-up-an-eerily-intense-totally-unblinking-cameo-in-the-new-Star-Wars-7-movie/")
#print alchemyapi.concepts("url", "http://www.argos.co.uk/static/Product/partNumber/1023828.htm")


