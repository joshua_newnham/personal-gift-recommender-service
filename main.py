#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import csv
import webapp2
from google.appengine.ext import blobstore
from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.ext import db
from handlers import *
from model import *

class MainHandler(webapp2.RequestHandler):
    def get(self):
        self.response.write('Hello world!')

app = webapp2.WSGIApplication([
        ('/service/', MainHandler),
        ('/service/user/details', UserHandler),
        ('/service/user/update', UserUpdateHandler),
        ('/service/user/updatestatus', UserUpdateStatusHandler),
        ('/service/user/recommendedproducts', UserRecommendedProductsHandler), 
        ('/service/user/addproducts', UserAddProductsHandler),
        ('/service/csvupload', CSVUploadHandler),
    ], debug=True)
